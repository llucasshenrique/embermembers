import { ReceiverMailingAddress } from "./receiver-mailing-address";

export interface Order {
    isInternational: boolean;
    from: string;
    to: string;
    memberID: string;
    planID?: string;
    productID: string;
    mailingAddress: ReceiverMailingAddress;
    trackingCode: string;
    carrier: string;
}