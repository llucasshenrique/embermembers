export interface ReceiverMailingAddress {
    isMainAddress: boolean;
    zipCode: number;
    address: string;
    houseNumber: number;
    city: string;
    state: string;
    country: string;
}
