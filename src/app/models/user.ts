export class User {
	uid: string;
	name: string;
	role: string;
	constructor(name: string, role: string) {
		this.name = name;
		this.role = role;
	}
}
