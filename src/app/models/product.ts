export interface Product {
	$key: string;
	sku: string;
	price: number;
	name: string;
	description: string;
	toMembers: boolean;
	membership: string;
}
