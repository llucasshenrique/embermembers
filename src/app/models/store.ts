export interface Store extends Object {
	name: string;
	description: string;
	icon: string;
	creator: string;
}
export interface StoreID extends Store { id: string; }
