import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';

import { StoreService } from './services/store/store.service';
import { UserService } from './services/user/user.service';
<<<<<<< HEAD
import { AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import { LoginService } from './services/login/login.service';
=======
import { User } from './models/user';
>>>>>>> dae5dc3e41f1438af06a904efdca0cabf6f962b9

interface Page {
  name: string;
  path: string;
  icon?: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  pagesMenu: Page[] = [
    { name: 'Dashboard',  path: '',       icon: 'dashboard'},
    { name: 'Plan',       path: 'plan',   icon: 'assessment' },
    { name: 'Quota',      path: 'quota',  icon: 'loyalty' },
    { name: 'Order',     path: 'order',  icon: 'shopping_cart' },
    { name: 'Member',     path: 'member', icon: 'account_circle' }
   ];
  private profileImage: string;
  private profileBackground: string;
  private showLoginMenu: boolean;
   constructor(
     private userService: UserService,
     private loginService: LoginService,
     private afAuth: AngularFireAuth) {
       afAuth.authState.subscribe(login => {
         console.log(status);
         if (login) { this.showLoginMenu = false; }
        });
      }
  ngOnInit() { }
  signOut() { this.loginService.signOut(); }
}
