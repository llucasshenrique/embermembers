import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';

import 'hammerjs';

import {
  MatButtonModule,
  MatCardModule,
  MatChipsModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatSidenavModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule
} from '@angular/material';

import { AppComponent } from './app.component';
import { StoreService } from './services/store/store.service';
import { UserService } from './services/user/user.service';
import { LoginService } from './services/login/login.service';
import { environment } from '../environments/environment';

import { routing } from './app.routing';
import { LoginComponent } from './components/login/login.component';
import { StoreListComponent } from './components/store-list/store-list.component';
import { StoreNewComponent } from './components/store-new/store-new.component';
import { StoreDetailComponent } from './components/store-detail/store-detail.component';
import { SidebarPanelComponent } from './components/sidebar-panel/sidebar-panel.component';
import { PlanComponent } from './components/plan/plan.component';
import { MemberComponent } from './components/member/member.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { StoreComponent } from './components/store/store.component';
import { OrderComponent } from './components/order/order.component';
import { DashboardSelectionMenuComponent } from './components/dashboard-selection-menu/dashboard-selection-menu.component';
import { ProductComponent } from './components/product/product.component';
import { QuotaComponent } from './components/quota/quota.component';
import { HeaderComponent } from './components/header/header.component';
import { SignupComponent } from './components/signup/signup.component';
import { SigninComponent } from './components/signin/signin.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    StoreListComponent,
    StoreNewComponent,
    StoreDetailComponent,
    SidebarPanelComponent,
    PlanComponent,
    MemberComponent,
    DashboardComponent,
    StoreComponent,
    OrderComponent,
    DashboardSelectionMenuComponent,
    ProductComponent,
    QuotaComponent,
    HeaderComponent,
    SignupComponent,
    SigninComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule.enablePersistence(),
    AngularFireAuthModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    routing,
    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule
  ],
  providers: [
    UserService,
    LoginService,
    StoreService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
