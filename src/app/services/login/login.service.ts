import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class LoginService {
  constructor(private afAuth: AngularFireAuth) { }
  login(email: string, password: string) {
    this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }
  signOut() {
    this.afAuth.auth.signOut();
  }
}
