import { Injectable } from '@angular/core';

import { AngularFirestore } from 'angularfire2/firestore';
import { User } from '../../models/user';

@Injectable()
export class UserService {
  
  constructor(private afFirestore: AngularFirestore) {
  }
  create(userData: User) {
    this.afFirestore.collection('user').add(userData);
  }
  read(userID: string) {
    return this.afFirestore.doc(`/user/${userID}`).valueChanges();
  }
  update(userID: string, userData: User) {
    this.afFirestore.doc(`/user/${userID}`).update(userData);
  }
  delete(userID: string) {
    this.afFirestore.doc(`/user/${userID}`).delete();
  }
}
