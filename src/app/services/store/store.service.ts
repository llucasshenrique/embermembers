import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Store, StoreID } from '../../models/store';

@Injectable()
export class StoreService {

  constructor(private afFirestore: AngularFirestore) { }
  create(storeData: Store) {
    this.afFirestore.collection('store').add(storeData);
  }
  read() {
    return this.afFirestore.collection('store');
  }
  getByKey(storeKey: string) {
    return this.afFirestore.doc(`/store/${storeKey}`)
  }
  update(storeData: StoreID) {
    const storeRefference = this.afFirestore.doc(`/store/${storeData.id}`);
    if (delete storeData.id) {
      storeRefference.update(storeData);
        // storeRefference.update({
        // name: storeData.name,
        // description: storeData.description,
        // icon: storeData.icon || null,
        // creator: storeData.creator || null
      // });
    }
  }
  delete(storeID: string) {
    this.afFirestore.doc(`/store/${storeID}`).delete();
  }
}
