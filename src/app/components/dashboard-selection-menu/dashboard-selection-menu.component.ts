import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-selection-menu',
  templateUrl: './dashboard-selection-menu.component.html',
  styleUrls: ['./dashboard-selection-menu.component.css']
})
export class DashboardSelectionMenuComponent implements OnInit {
  plansSelection = [
    { name: 'Epilepsia', id: '12asda5'},
    { name: 'Dravet', id: '3213dsa'},
    { name: 'Pânico', id: '232cddcasdas'}
  ];
  storesSelection = [
    { name: 'Associados', id: 'ass321' },
    { name: 'Não Associados', id: 'nass111323' },
    { name: 'Souveniers', id: 'souvasd04536'}
  ];
  constructor() { }

  ngOnInit() {
  }

}
