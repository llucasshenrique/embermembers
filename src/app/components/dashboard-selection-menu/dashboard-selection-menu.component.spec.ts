import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardSelectionMenuComponent } from './dashboard-selection-menu.component';

describe('DashboardSelectionMenuComponent', () => {
  let component: DashboardSelectionMenuComponent;
  let fixture: ComponentFixture<DashboardSelectionMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardSelectionMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardSelectionMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
