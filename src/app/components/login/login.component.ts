import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  isLoggedIn: boolean;
  private formSubmitAttempt: boolean;
  constructor(
    private afAuth: AngularFireAuth,
    private formBuilder: FormBuilder) { }
  ngOnInit() {
    this.afAuth.authState.subscribe(user => {
      console.log(user);
      if ( !user ) {
        this.isLoggedIn = false;
      } else {
        this.isLoggedIn = true;
      }
    }),
    this.form = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
      });
  }
  isFieldInvalid(field: string) {
    return (
      (!this.form.get(field).valid && this.form.get(field).touched) ||
      (this.form.get(field).untouched && this.formSubmitAttempt)
    );
  }
  login() {
    if (this.form.valid) {
      this.afAuth.auth.signInWithEmailAndPassword(
        this.form.value.email,
        this.form.value.password
      );
    }
    this.formSubmitAttempt = true;
  }
}
