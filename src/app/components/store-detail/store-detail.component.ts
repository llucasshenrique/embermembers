import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-store-detail',
  templateUrl: './store-detail.component.html',
  styleUrls: ['./store-detail.component.css']
})
export class StoreDetailComponent implements OnInit {
  id: string;
  constructor(private activatedRoute: ActivatedRoute) {
    this.id = activatedRoute.snapshot.params['id'];
    console.log(activatedRoute);
  }

  ngOnInit() {
  }

}
