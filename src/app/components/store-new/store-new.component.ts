import { Component, OnInit } from '@angular/core';
import { Store } from '../../models/store';
import { StoreService } from '../../services/store/store.service';

@Component({
  selector: 'app-store-new',
  templateUrl: './store-new.component.html',
  styleUrls: ['./store-new.component.css']
})
export class StoreNewComponent implements OnInit {
  constructor(private storeService: StoreService) { }
  ngOnInit() {
  }
  createStore(storeData) {
    console.log(storeData.value);
    this.storeService.create(storeData.value);
  }
}
