import { AngularFirestoreCollection } from 'angularfire2/firestore';
import { StoreService } from './../../services/store/store.service';
import { StoreID, Store } from './../../models/store';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';


export class TableStoreDataSource extends DataSource<any> {
  constructor(private afFirestore ) {
    super();
  }
  connect(): Observable<StoreID[]> {
    return this.afFirestore.read().valueChanges();
  }
  disconnect() {}
}

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {
  storeCol: AngularFirestoreCollection<any>;
  stores: Observable<StoreID[]>;
  displayedColumns = ['name', 'description'];
  dataSource: TableStoreDataSource | null;
  constructor(private storeService: StoreService) {    
    this.dataSource = new TableStoreDataSource(storeService);
  }

  ngOnInit() {
    this.storeCol = this.storeService.read();
    this.stores = this.storeCol.snapshotChanges().map(actions => {
      return actions.map(action => {
        const data = action.payload.doc.data() as Store;
        const id = action.payload.doc.id;
        return { id, ...data };
      });
    });
  }
}
