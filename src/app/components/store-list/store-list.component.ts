import { Component, OnInit, ViewChild } from '@angular/core';
import { StoreService } from '../../services/store/store.service';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import { StoreID, Store } from '../../models/store';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';
import 'rxjs/add/observable/of';
import { MatSort } from '@angular/material';



@Component({
  selector: 'app-store-list',
  templateUrl: './store-list.component.html',
  styleUrls: ['./store-list.component.css']
})
export class StoreListComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  storeCol: AngularFirestoreCollection<any>;
  stores: Observable<StoreID[]>;
  displayedColumns = ['name', 'description'];

  constructor(private storeService: StoreService) {
  }

  ngOnInit() {
  }
  updateStore(store) {
    console.log(store);
    this.storeService.update(store);
  }
  deleteStore(storeID: StoreID) {
    this.storeService.delete(storeID.id);
  }
}
