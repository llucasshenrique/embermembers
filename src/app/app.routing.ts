import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { StoreListComponent } from './components/store-list/store-list.component';
import { LoginComponent } from './components/login/login.component';
import { PlanComponent } from './components/plan/plan.component';
import { MemberComponent } from './components/member/member.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { QuotaComponent } from './components/quota/quota.component';
import { OrderComponent } from './components/order/order.component';
import { SignupComponent } from './components/signup/signup.component';


const APP_ROUTES: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'quota', component: QuotaComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'member', component: MemberComponent },
  { path: 'order', component: OrderComponent },
  { path: 'plan', component: PlanComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);
