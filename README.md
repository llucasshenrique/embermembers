## ToDos

- [ ] SignUp page

- [ ] Production
  - [ ] Product
  - [ ] Requirements
  - [ ] Process


An proccess is a group of tasks.

- [ ] Task
  - [ ] Duration
  - [ ] Requirements
  - [ ] Results

Production Example
  - Production: Electric Car

  | Process | Requirements |
  | --- | --- | 
  | Assembly ||
  | Piece Request ||
  | Product Storage ||
  || Car Plan |
  || car body |
  || transmition system |
  || tires |
  || computer |
  || electric engine |
  || battery |
  || direction system |

Task
  - Task: Piece Request: 16" tires, 4

  | Requirement | Result  |
  | ---         | ---     |
  | 4 tires, size 16" | Avaibility |

  - Task: Product Storage

  | Requirement | Result  |
  | ---         | ---     |
  | Storage Space | Avaibility |
  In this case the pieces selection can be done in any time because 